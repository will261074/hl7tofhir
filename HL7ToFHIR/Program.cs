﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Hl7.Fhir.Rest;
using HL7ToFhire;
using Hl7.Fhir.Model;
using System.ComponentModel;

namespace HL7ToFHIR
{

    public class Program
    {


        public static void Main()
        {

            //Read in message
            var files = Directory.GetFiles(@"C:\Users\WilliamGlover\Restart Consulting Dropbox\William Glover\C#\Testing\HL7\In");

            foreach (var file in files)
            {

                string hl7String = File.ReadAllText(file);

                var msg = Util.BuildHL7vDoc(hl7String);

                List<Patient> resources = Transform.ParentTransform(msg);

                foreach(var fhirResource in resources)
                {

                    FhirREST.AddPatientResource(fhirResource);

                    }
                }
            }
        }
    }
  
