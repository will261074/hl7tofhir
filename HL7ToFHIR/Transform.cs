﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Policy;
using System.Text;
using Hl7.Fhir.Model;
using NHapi.Model.V21.Message;
using NHapi.Model.V27.Segment;
using NHapi.Model.V26.Group;
using System.ComponentModel;

namespace HL7ToFHIR
{

    public class Transform
    {
        //This would be setup as a class eventually so all A01/A05 ect derivatives can just inherit from them
        //Couldnt find a way to loop through the segements, if could, then would avoid class interface entriely and
        //And juest build off of switch statements to call individual segment transforms
        private static string[] _a01 = { "MSH", "PID", "PV1" };
        

        public static List<Patient> ParentTransform(NHapi.Base.Model.IMessage msg) {

            string[] segementAry;
            List<Patient> resources = new List<Patient>();

            switch (msg.GetStructureName())
            {

                case "ADT_A01":
                    segementAry = _a01;
                    break;
                default:
                    Console.WriteLine("Message structure not retrievable");
                    return resources;
            }
            //loop through them and map to FHIR resource required
            foreach (var segment in segementAry)
            {
                resources.Add(AssessAndMapSegement(segment, msg));
            }

            return resources;
        
        }

        public static Patient AssessAndMapSegement(string segment, NHapi.Base.Model.IMessage msg) {

            Patient output = new Patient();

            switch (segment) {
                case "PID":
                    output = PIDToPatientResource(msg.GetStructure("PID") as PID);
                    break;
                default:
                    Console.WriteLine("Wills Hl7 to FHIR converter cannot process this segement at this time");
                    break;
            }

            return output;
        
        }

   
        public static Patient PIDToPatientResource(PID segment)
        {
            var patient = new Patient();

            var id = new Identifier();
            id.System = "NHS";
            id.Value = segment.GetPatientIdentifierList(1).ToString();
            patient.Identifier.Add(id);

            var name = new HumanName().WithGiven(segment.GetPatientName(1).ToString()).WithGiven(segment.GetPatientName(3).ToString());
            name.Prefix = new string[] { segment.GetPatientName(5).ToString() };
            name.Use = HumanName.NameUse.Official;
            patient.Name.Add(name);


            patient.BirthDate = segment.DateTimeOfBirth.ToString();

            //cannot cast between 2 so used shorthand as messaegs onll hold these 2 values
            AdministrativeGender sex = segment.AdministrativeSex.ToString() == "M" ? AdministrativeGender.Male : AdministrativeGender.Female;
            patient.Gender = sex;


            string[] addressAry = segment.GetPatientAddress(1).ToString().Split("^");
            var address = new Address()
            {
                Line = new string[] { addressAry[1], addressAry[2] },
                State = addressAry[4],
                PostalCode = addressAry[5],
                Country = addressAry[6]
            };
            patient.Address.Add(address);


            patient.Telecom.Add(new ContactPoint { System = ContactPoint.ContactPointSystem.Phone, Value = segment.GetPhoneNumberHome().ToString(), Use = ContactPoint.ContactPointUse.Home });
            patient.Telecom.Add(new ContactPoint { System = ContactPoint.ContactPointSystem.Phone, Value = segment.GetPhoneNumberBusiness().ToString(), Use = ContactPoint.ContactPointUse.Work });

            bool isDead = segment.PatientDeathIndicator.ToString() == "N" ? false : true;
            patient.Deceased = new FhirBoolean(isDead);

            Console.WriteLine(patient.ToString());

            return patient;
        }



    }
    
 
}
