﻿using NHapi.Base.Parser;
using NHapi.Model.V21.Segment;
using NHapi.Model.V21.Message;
using Microsoft.VisualBasic;
using System;
using System.Linq.Expressions;

namespace HL7ToFhire
{

    public class Util
    {



        public static void Test()
        {
            string msg = @"MSH|^~\&|SendingApp|SendingFacility|HL7API|PKB|20160102101112||ADT^A01|ABC0000000001|P|2.4|\n" +
                         "PID|||3333333333^^^NHS^NH||Johnson^^Kay^^Mrs||19401222|F|||^9 Unknown Lane^St Albans^Hertfordshire^AL1 3JE^GBR||02034117899^PRN~07893292225^PRS|^NET^some - email@outlook.com||||||||||||||||N|\n" +
                         "PV1|1|I|^^^^^^^^My Ward||||^Jones^Stuart^James^^Dr^|^Smith^William^^^Dr^|^Foster^Terry^^^Mr^||||||||||V00001|||||||||||||||||||||||||201508011000|201508011200";

            BuildHL7vDoc(msg);
        }


        public static NHapi.Base.Model.IMessage BuildHL7vDoc(string hl7String)
        {

            var pipeParser = new PipeParser();
            var hl7Message = pipeParser.Parse(hl7String);

            return hl7Message;
        }

    }
}
